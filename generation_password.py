import string
import random

def generate_password(number = 8):
	s = []
	# Наполнение списка необходимыми наборами символов:
	# буквами латинского алфавита
	s.append(string.ascii_letters)
	# цифрами
	s.append(string.digits)
	# спецсимволами
	s.append(string.punctuation)
	# Объединение их в одну строку и затем разбивка на отдельные символы
	separate_symbols = list(''.join(s))
	# Выбор из полученного списка случайных символов в количестве равном требуемой длине пароля
	random_symbols = [random.choice(separate_symbols) for x in range(number)]
	# Полученная строка с паролем
	return ''.join(random_symbols)

def print_password(number):
	# Вывод в терминал
	print(generate_password(number))

if __name__ == '__main__':
	print_password()